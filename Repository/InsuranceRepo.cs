﻿using MappleSoftware.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MappleSoftware.Repository
{
    public class InsuranceRepo
    {
        SqlConnection con = new SqlConnection();
        public int SchemeRepo(Customer _customer)
        {
            using (SqlConnection con = new SqlConnection(Startup.connectionString))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand("USP_ManageLifeInsuranceContract", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@intType", _customer.Type);
                    cmd.Parameters.AddWithValue("@customerId", _customer.customerId == null ? 0 : _customer.customerId);
                    cmd.Parameters.AddWithValue("@customerName", _customer.customerName == null ? "" : _customer.customerName);
                    cmd.Parameters.AddWithValue("@customerGender", _customer.customerGender == null ? "" : _customer.customerGender);
                    cmd.Parameters.AddWithValue("@customerCountry", _customer.customerCountry == null ? "" : _customer.customerCountry);
                    cmd.Parameters.AddWithValue("@customerDOB", _customer.customerDOB == null ? System.DateTime.Now : _customer.customerDOB);
                    cmd.Parameters.AddWithValue("@salesDate", _customer.salesDate == null ? System.DateTime.Now : _customer.salesDate);
                   
                    return cmd.ExecuteNonQuery();
                }
            }
        }
        public List<Customer> ListSchemeRepo(Customer _customer)
        {
            using (SqlConnection con = new SqlConnection(Startup.connectionString))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand("USP_GetLifeInsuranceContract", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                  
                    SqlDataReader sdr = cmd.ExecuteReader();
                    List<Customer> lst = new List<Customer>();
                    while (sdr.Read())
                    {
                        lst.Add(new Customer
                        {
                            customerName=Convert.ToString(sdr["customerName"]),
                            customerGender = Convert.ToString(sdr["customerGender"]),
                            customerCountry = Convert.ToString(sdr["customerCountry"]),
                            customerDOB = Convert.ToDateTime(sdr["customerDOB"]),
                            salesDate = Convert.ToDateTime(sdr["salesDate"])
                        });
                    }
                    con.Close();
                    return lst;
                }
            }
        }

        public int DeleteSchemeRepo(long CustomerId)
        {
            using (SqlConnection con = new SqlConnection(Startup.connectionString))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand("USP_DeleteLifeInsuranceContract", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@customerId", CustomerId);
                    con.Open();
                    return cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
