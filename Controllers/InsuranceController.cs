﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MappleSoftware.Models;
using MappleSoftware.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace MappleSoftware.Controllers
{
    [Route("api/Insurance")]
    [ApiController]
    public class InsuranceController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public InsuranceController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        InsuranceRepo IRP=new InsuranceRepo();
        [HttpPost("api/Insurance/new")]
        public IActionResult AddNewContract(Customer customer)
        {
            try
            {
                customer.Type = 0;
                return Ok(IRP.SchemeRepo(customer));
            }
            catch(Exception ex) {
                return BadRequest($"Failed to create new Contract");
            }
        }
        [HttpPut("api/Insurance/update")]
        public IActionResult UpdateContract(Customer customer)
        {
            try
            {
                customer.Type = 1;
                return Ok(IRP.SchemeRepo(customer));
            }
            catch (Exception ex)
            {
                return BadRequest($"Failed to update Contract");
            }
        }

        [HttpDelete("api/Insurance/delete")]
        public IActionResult DeleteContract(long customerId)
        {
            try
            {
                return Ok(IRP.DeleteSchemeRepo(customerId));
            }
            catch (Exception ex)
            {
                return BadRequest($"Failed to delete Contract");
            }
        }
        [HttpGet("api/Insurance/list")]
        public IActionResult list()
        {
            try
            {
                Customer customer = new Customer();
                customer.Type = 3;
                customer.customerId = 0;
                return Ok(IRP.ListSchemeRepo(customer));
            }
            catch (Exception ex)
            {
                return BadRequest($"Failed to fetch Contract");
            }
        }
    }
}