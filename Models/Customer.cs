﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MappleSoftware.Models
{
    public class Customer
    {
        public long customerId { get; set; }
        public string customerName { get; set; }
        public string customerGender { get; set; }
        public string customerCountry { get; set; }
        public DateTime customerDOB { get; set; }
        public DateTime salesDate { get; set; }
        public string coveragePlan { get; set; }
        public decimal netPrice { get; set; }

        public int Type { get; set; }
    }
}
